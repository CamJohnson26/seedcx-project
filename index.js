import Express from 'express';
import React from 'react'
import Page from './seedcx/components/Page.jsx';
import { renderToString } from 'react-dom/server';
import App from './seedcx/components/App.jsx';
import { StaticRouter } from 'react-router-dom';
const passport = require('passport');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var flash = require('connect-flash');
var session = require('express-session');
var settings = require('./seedcx/config/settings');
var jwt = require('jsonwebtoken');
var User = require('./seedcx/models/User');
var Transaction = require('./seedcx/models/Transaction');
var csv = require('express-csv');

const app = new Express();

const port = process.env.PORT || 3000;
const env = process.env.NODE_ENV || 'production';

app.set('view engine', 'ejs')
app.set('views', __dirname + '/seedcx/views')

app.listen(port, (error) => {
	if (error) {
		return console.log('Error: ', error)
	}
	console.log(`Server running on http://localhost:${port} [${env}]`)
})

app.use(Express.static('seedcx/resources'));

require('./seedcx/config/passport')(passport);

app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(flash());


app.post('/api/auth/register', function(req, res) {
    if(!req.body.email || !req.body.password) {
      res.json({success: false, msg: 'Missing email or password.'});
    } else {
      var newUser = new User(req.body.email, req.body.password);
      newUser.save(function(err) {
        if (err) {
          return res.json({success: false, msg: 'Email already registered.'});
        }
        res.json({success: true, msg: 'Successfully created user.'});
      })
    }
  }
);

app.post('/api/auth/login', function(req, res) {
    User.findOne(req.body.email, function(err, user) {
      if (err) {
        throw err;
      }
      if (!user) {
        res.status(401).send({success: false, msg: 'Authentication failed. User not found.'});
      } else {
        user.comparePassword(req.body.password, function (err, isMatch) {
          if (isMatch && !err) {
            var token = jwt.sign(JSON.stringify(user), settings.secret);
            res.json({success: true, token: 'JWT ' + token});
          } else {
            res.status(401).send({success: false, msg: 'Authentication failed. Invalid password.'});
          }
        })
      }
    })
  }
);

app.post('/api/transaction', passport.authenticate('jwt', {session: false}), function(req, res) {
    let token = getToken(req.headers);
    let payload = jwt.verify(token, settings.secret);
    if (!payload.admin) {
      return res.status(401).send({success: false, msg: 'Unauthorized'});
    }

    let amount = req.body.amount;
    let coinid = req.body.coinid;
    let date = new Date();
    let pending = true;
    let userid = payload.userid

    let transaction = new Transaction(userid, coinid, pending, amount, date)
    transaction.save(function(result, err) {
        if (err) {
          return res.json({success: false, msg: 'Email already registered.'});
        }
        return res.json({success: true, msg: JSON.stringify(result)});
    })
  }
);

app.get('/api/auth/logout', function(req, res) {
    req.logout();
    res.redirect('/');
});

app.get('/api/history', passport.authenticate('jwt', {session: false}), function(req, res) {
  var token = getToken(req.headers);
  var payload = jwt.verify(token, settings.secret);
  if (token) {
    Transaction.find(payload.email, -1, function(err, transactions) {
      if (err) {
        throw err;
      }
      res.json({success: true, msg: JSON.stringify(transactions)});
    })
  } else {
    return res.status(403).send({success: false, msg: token});
  }
});

app.get('/api/account', passport.authenticate('jwt', {session: false}), function(req, res) {
  var token = getToken(req.headers);
  var payload = jwt.verify(token, settings.secret);
  if (token) {
    Transaction.balances(payload.email, function(err, balances) {
      if (err) {
        throw err;
      }
      res.json({success: true, msg: JSON.stringify(balances)});
    })
  } else {
    return res.status(403).send({success: false, msg: token});
  }
});

app.get('/api/account/export', passport.authenticate('jwt', {session: false}), function(req, res) {
  var token = getToken(req.headers);
  var payload = jwt.verify(token, settings.secret);
  if (token) {
    Transaction.balances(payload.email, function(err, balances) {
      if (err) {
        throw err;
      }
          
      res.statusCode = 200;
      res.setHeader('Content-Type', 'text/csv');
      res.csv(balances, true);
    })
  } else {
    return res.status(403).send({success: false, msg: token});
  }
});

app.get('/api/history/export', passport.authenticate('jwt', {session: false}), function(req, res) {
  var token = getToken(req.headers);
  var payload = jwt.verify(token, settings.secret);
  if (token) {
    Transaction.find(payload.email, -1, function(err, transactions) {
      if (err) {
        throw err;
      }
          
      res.statusCode = 200;
      res.setHeader('Content-Type', 'text/csv');
      res.csv(transactions, true);
    })
  } else {
    return res.status(403).send({success: false, msg: token});
  }
});

app.get('/api', passport.authenticate('jwt', {session: false}), function(req, res) {
  var token = getToken(req.headers);
  var payload = jwt.verify(token, settings.secret);

  if (token) {
    res.status(200).send({success: true, msg: {token: token, admin: payload.admin}});
  } else {
    return res.status(403).send({success: false, msg: token});
  }
});

app.get('*', (req, res) => {
  var context = {};
  return res.render('index', { "markup": renderToString(<StaticRouter location={req.url} context={context}><App /></StaticRouter>) });
});

function getToken(headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ');
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
}
