var webpack = require('webpack');

process.env.BROWSER = true;

module.exports = options => {
  return {
    target: 'web',
    entry: './seedcx/index.jsx',
    output: {
		path: __dirname + '/seedcx/resources/js',
      filename: 'bundle.js',
    },
    module: {
      rules: [
        {
  		  test: /\.(js|jsx)$/,
            exclude: /node_modules/,
            use: [
              {
                loader: 'babel-loader',
                options: {
                  cacheDirectory: true,
                },
              },
            ],
        },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      ],
    },
    plugins: [
      new webpack.DefinePlugin({
          "process.env.BROWSER": true,
      })
    ],
    node: {
      fs: 'empty'
    },
  }
}