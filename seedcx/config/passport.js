var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;

var settings = require('../config/settings');

var User = require('../models/User');

module.exports = function(passport) {

    var opts = {}
    opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme("jwt");
    opts.secretOrKey = settings.secret;

    passport.use(new JwtStrategy(opts, function(jwt_payload, done) {
        User.findOne(jwt_payload.email, function(err, user) {
            if (err) {
                return done(err, false);
            }
            if (user) {
                done(null, user);
            } else {
                done(null, false)
            }
        })
    }))
};