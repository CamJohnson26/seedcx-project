import { Link } from 'react-router-dom'
import React from 'react';
import { withRouter, Redirect } from 'react-router-dom'
import axios from 'axios';
const FileDownload = require('react-file-download');

if (process.env.BROWSER) {
    require("../resources/css/LoginPage.css");
}

const api = {
  getAccounts(component) {
    axios.defaults.headers.common['Authorization'] = localStorage.getItem('jwtToken');
    axios.get('/api/account')
      .then((res) => {
        component.setState({balances: JSON.parse(res.data.msg)});
      })
      .catch((error) => {
        if (error.response.status === 401) {
        this.auth = false;
          component.setState({transactions: []});
        }
      })
  },
  signout() {
    localStorage.removeItem('jwtToken');
    window.location.reload();
  }
}

export default class AccountsPage extends React.Component {
    constructor(props) {
        super(props);

        if (typeof window !== 'undefined') {
          api.getAccounts(this);
        }

        this.state = {
            balances: []
        };
    }
    exportAccount() {
      axios.get('/api/account/export')
      .then((res) => {
        FileDownload(res.data, 'seedcx-account.csv');
      })
      .catch((error) => {
        if (error.response.status === 401) {
          console.log(error);
        }
      })
    }
  render() {
    console.log(this.props)
    return (
      <div>
        <div className="right-action">
          <div className="btn-group float-right">
            <button type="button" class="btn btn-primary " onClick={this.exportAccount}>
              Export
            </button>
          </div>
        </div>
        <table className="table table-striped">
            <thead>
              <tr>
                <th>Coin</th>
                <th>Balance</th>
                {this.props.isAdmin ? <th>Link</th> : null}
              </tr>
            </thead>
            <tbody>

              {this.state.balances.map(function(balance){
                return <tr>
                    <td>{balance.abbreviation}</td>
                    <td><Link to={'/history/' + balance.abbreviation}>{parseFloat(balance.balance).toFixed(6)}</Link></td>
                    {this.props.isAdmin ? <td><Link to={'/transactions/' + balance.abbreviation}><span className="badge">Deposits & Withdrawals</span></Link></td> :null}
                  </tr>;
              }, this)}
            </tbody>
          </table>
      </div>
    );
  }
}