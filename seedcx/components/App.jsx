import { Link, Route, Redirect} from 'react-router-dom'
import AccountsPage from './AccountsPage.jsx'
import HistoryPage from './HistoryPage.jsx'
import TransactionsPage from './TransactionsPage.jsx'
import LoginPage from './LoginPage.jsx'
import React from 'react';
var request = require('request');
import axios from 'axios';

const auth = {
  authenticate(component) {
    axios.defaults.headers.common['Authorization'] = localStorage.getItem('jwtToken');
    axios.get('/api')
      .then((res) => {
        this.auth = true;
        component.setState({isAuthenticated: true});
        component.setState({isAdmin: res.data.msg.admin});
      })
      .catch((error) => {
        if (error.response.status === 401) {
        this.auth = false;
          component.setState({isAuthenticated: false});
        }
      })
  },
  signout() {
    localStorage.removeItem('jwtToken');
    window.location.reload();
  }
}

const PrivateRoute = ({ component: Component, extraProps, ...rest }) => (
  <Route {...rest} render={(props) => {
    return auth.auth === true
      ? <Component {...props}  {...extraProps}/>
      : <Redirect to='/login' />
  }
  } />
)

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        isAuthenticated: false,
        isAdmin: false
    };
    if (typeof window !== 'undefined') {
      auth.authenticate(this);
    }
  }
  render() {
    return (
      <div>
      <nav className="navbar navbar-expand-sm bg-dark navbar-dark">
        <div className="container-fluid">
          {
            this.state.isAuthenticated ?
            <ul className="navbar-nav">
              <p className="navbar-brand">SeedCX Project</p>
              <li className="nav-item"><Link className="nav-link" to='/accounts'>Accounts</Link></li>
              <li className="nav-item"><Link className="nav-link" to='/history'>History</Link></li>
              { this.state.isAdmin ? <li className="nav-item"><Link className="nav-link" to='/transactions'>Transactions</Link></li> : null}
            </ul>
            :
            <ul className="navbar-nav">
              <p className="navbar-brand">SeedCX Project</p>
              <li className="nav-item"><Link className="nav-link" to='/login'>Login</Link></li>
            </ul>
          }
          {
            this.state.isAuthenticated ? <button className="btn btn-dark" onClick={auth.signout}>Logout</button> : null
          }
        </div>
      </nav>
        <div>
        {
          this.state.isAuthenticated ?
          <Redirect to="/accounts" />
          :
          <Route exact={true} path="/login" component={LoginPage}/>
        }
          <PrivateRoute path="/accounts" component={AccountsPage} extraProps={{isAdmin: this.state.isAdmin}}/>
          <PrivateRoute path="/history/:coin?" component={HistoryPage} extraProps={{isAdmin: this.state.isAdmin}} />
          { this.state.isAdmin ? <PrivateRoute path="/transactions/:coin?" component={TransactionsPage}/> : null}
        </div>
      </div>
    );
  }
}