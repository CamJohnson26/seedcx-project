import React from 'react';

export default class Page extends React.Component {
  render() {
    return (
      <div className="home display-1"><h1>Hello React!</h1></div>
    );
  }
}