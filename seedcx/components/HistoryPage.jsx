import React from 'react';
import { withRouter, Redirect } from 'react-router-dom'
import axios from 'axios';
const FileDownload = require('react-file-download');
import { matchPath } from 'react-router'


if (process.env.BROWSER) {
    require("../resources/css/LoginPage.css");
}

const api = {
  getHistory(component) {
    axios.defaults.headers.common['Authorization'] = localStorage.getItem('jwtToken');
    axios.get('/api/history')
      .then((res) => {
        component.setState({cachedTransactions: JSON.parse(res.data.msg)});
        component.setState({transactions: JSON.parse(res.data.msg).filter((r) => r.abbreviation === component.state.abbreviation)})
      })
      .catch((error) => {
        if (error.response.status === 401) {
          component.setState({cachedTransactions: []});
        }
      })
  },
}

export default class HistoryPage extends React.Component {
    constructor(props) {

        super(props);

        let abbreviation = 'USD';
        let coinid = 1;

        if (this.props.match.params.coin) {
            abbreviation = this.props.match.params.coin;
        }
        
        if (abbreviation === 'USD') {
            coinid = 1;
        } else if (abbreviation === 'BTC') {
            coinid = 2;
        } else if (abbreviation === 'ETH') {
            coinid = 3;
        } else {
            abbreviation = 'USD';
            coinid = 1;
        }

        if (typeof window !== 'undefined') {
          api.getHistory(this);
        }

        this.state = {
            cachedTransactions: [],
            transactions: [],
            coinid: coinid,
            abbreviation: abbreviation
        };

        this.dropdownSelect = this.dropdownSelect.bind(this);
    }

    dropdownSelect(e) {
        if (e.target.text === 'USD') {
            this.setState({coinid: 1, abbreviation: e.target.text});
        } else if (e.target.text === 'BTC') {
            this.setState({coinid: 2, abbreviation: e.target.text});
        } else if (e.target.text === 'ETH') {
            this.setState({coinid: 3, abbreviation: e.target.text});
        }
        this.setState({transactions: this.state.cachedTransactions.filter((r) => r.abbreviation === e.target.text)})
    }
    exportHistory() {
      axios.get('/api/history/export')
      .then((res) => {
        FileDownload(res.data, 'seedcx-history.csv');
      })
      .catch((error) => {
        if (error.response.status === 401) {
          console.log(error);
        }
      })
    }
  render() {
    return (
        <div>
            <div className="right-action">
                <div className="btn-group float-right">
                    <div class="dropdown btn-group">
                      <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                        {this.state.abbreviation}
                      </button>
                      <div class="dropdown-menu">
                        <a class="dropdown-item" onClick={this.dropdownSelect} href="#">USD</a>
                        <a class="dropdown-item" onClick={this.dropdownSelect} href="#">BTC</a>
                        <a class="dropdown-item" onClick={this.dropdownSelect} href="#">ETH</a>
                      </div>
                    </div>
                    <button type="button" class="btn " onClick={this.exportHistory}>
                      Export
                    </button>
                </div> 
            </div>
        <table className="table table-striped">
            <thead>
              <tr>
                <th>Status</th>
                <th>Amount</th>
                <th>Coin</th>
                <th>Date</th>
              </tr>
            </thead>
            <tbody>

              {this.state.transactions.map(function(transaction){
                return <tr>
                    <td>{
                        transaction.pending ? <span class="badge badge-warning">PENDING</span>
                        : <span class="badge badge-success">APPROVED</span>
                    }</td>
                    <td>{parseFloat(transaction.amount).toFixed(6)}</td>
                    <td>{transaction.abbreviation}</td>
                    <td>{transaction.date}</td>
                  </tr>;
              }, this)}
            </tbody>
          </table>
          </div>
    );
  }
}