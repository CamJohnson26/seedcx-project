import React from 'react';
import { withRouter, Redirect } from 'react-router-dom'
import axios from 'axios';

if (process.env.BROWSER) {
    require("../resources/css/LoginPage.css");
}

export default class TransactionsPage extends React.Component {
    constructor(props) {

        super(props);

        let abbreviation = 'USD';
        let coinid = 1;

        if (this.props.match.params.coin) {
            abbreviation = this.props.match.params.coin;
        }
        
        if (abbreviation === 'USD') {
            coinid = 1;
        } else if (abbreviation === 'BTC') {
            coinid = 2;
        } else if (abbreviation === 'ETH') {
            coinid = 3;
        } else {
            abbreviation = 'USD';
            coinid = 1;
        }

        this.state = {
            amount: 0,
            coinid: coinid,
            abbreviation: abbreviation
        };
        this.handleAmountChange = this.handleAmountChange.bind(this);
        this.dropdownSelect = this.dropdownSelect.bind(this);
        this.deposit = this.deposit.bind(this);
        this.withdraw = this.withdraw.bind(this);
    }
    handleAmountChange(e) {
        this.setState({amount: e.target.value});
    }
    dropdownSelect(e) {
        if (e.target.text === 'USD') {
            this.setState({coinid: 1, abbreviation: e.target.text});
        } else if (e.target.text === 'BTC') {
            this.setState({coinid: 2, abbreviation: e.target.text});
        } else if (e.target.text === 'ETH') {
            this.setState({coinid: 3, abbreviation: e.target.text});
        }
    }
    deposit() {
        axios.post('/api/transaction', {amount: this.state.amount, coinid: this.state.coinid})
            .then((result) => {
                localStorage.setItem('jwtToken', result.data.token);
                this.props.history.push("/history");
            })
            .catch((error) => {
                if (error.response.status === 401) {
                    this.setState({message: 'Login failed. Username or password invalid'});
                }
            });
    }
    withdraw() {
        const { email, password } = this.state;
        axios({
              method: 'post',
              url: '/api/transaction',
              data: {amount: -this.state.amount, coinid: this.state.coinid}
            })
            .then((result) => {
                this.props.history.push("/history")
            });
    }
    render() {
        return (
            <div className='right-action'>
                <div className="btn-group float-right">
                <div class="dropdown btn-primary btn-group">
                  <div class="dropdown-menu">
                    <a class="dropdown-item" onClick={this.dropdownSelect} href="#">USD</a>
                    <a class="dropdown-item" onClick={this.dropdownSelect} href="#">BTC</a>
                    <a class="dropdown-item" onClick={this.dropdownSelect} href="#">ETH</a>
                  </div>
                </div>
                  <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                    {this.state.abbreviation}
                  </button>
            </div> 
            <div className="home">
                <div className="wrapper">
                    <div className="form-signin">             
                        <input type="text" value={this.state.amount} onChange={ this.handleAmountChange } className="form-control" name="amount" placeholder="Amount" required="" autoFocus="" />
                        <button className="btn btn-lg btn-primary btn-block" onClick={ this.withdraw }>Withdraw</button> 
                        <button className="btn btn-lg btn-block" onClick={ this.deposit }>Deposit</button>   
                    </div>
                </div>
            </div>
            </div>
        );
    }
}