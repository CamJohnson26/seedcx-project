import React from 'react';
import { withRouter, Redirect } from 'react-router-dom'
import axios from 'axios';

if (process.env.BROWSER) {
    require("../resources/css/LoginPage.css");
}

class LoginPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            message: ''
        };

        this.login = this.login.bind(this);
        this.register = this.register.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
    }
    handleEmailChange(e) {
        this.setState({email: e.target.value});
    }
    handlePasswordChange(e) {
        this.setState({password: e.target.value});
    }
    login() {
        const { email, password } = this.state;

        axios.post('/api/auth/login', {email, password})
            .then((result) => {
                localStorage.setItem('jwtToken', result.data.token);
                this.setState({ message: ''});
                this.props.history.push("/accounts");
                window.location.reload();
            })
            .catch((error) => {
                if (error.response.status === 401) {
                    this.setState({message: 'Login failed. Username or password invalid'});
                }
            });
    }
    register() {
        const { email, password } = this.state;
        axios({
              method: 'post',
              url: '/api/auth/register',
              data: {email, password}
            })
            .then((result) => {
                this.props.history.push("/login")
            });
    }
    render() {
        return (
            <div className="home">
            <div className="wrapper">
            <div className="form-signin">             
            <h2 className="form-signin-heading">Please login</h2>
            <input type="text" value={this.state.email} onChange={ this.handleEmailChange } className="form-control" name="email" placeholder="Email Address" required="" autoFocus="" />
            <input type="password" value={this.state.password} onChange={ this.handlePasswordChange } className="form-control" name="password" placeholder="Password" required=""/>
            <button className="btn btn-lg btn-primary btn-block" onClick={ this.login }>Login</button> 
            <button className="btn btn-lg btn-block" onClick={ this.register }>Register</button>        
            </div>
            </div>
            </div>
            );
    }
}

export default withRouter(LoginPage);