import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import 'jquery/dist/jquery.js';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import App from './components/App.jsx';

window.onload = () => {
	ReactDOM.render(<BrowserRouter><App /></BrowserRouter>, document.getElementById('main'));
};