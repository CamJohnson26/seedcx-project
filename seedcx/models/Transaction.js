const pg = require('pg');

pg.defaults.ssl = true;

let pool = new pg.Pool({connectionString: process.env.DATABASE_URL}); 

class Transaction {

    constructor(userid, coinid, pending, amount, date) {
        this.userid = userid;
        this.coinid = coinid;
        this.pending = pending;
        this.amount = amount;
        this.date = date;
    }

    save(callback) {
        let userid = this.userid;
        let coinid = this.coinid;
        let pending = this.pending;
        let amount = this.amount;
        let date = this.date;
        pool.connect((err, client, done) => {
            if (err) {
                return console.error('Could not connect to Postgres.', err);
            }

            client.query('INSERT INTO transaction(userid, coinid, pending, amount, date) VALUES($1, $2, $3, $4, $5)', [userid, coinid, pending, amount, date], function (err, result) {
                done();
                if(err){
                    return console.error('error running query', err);
                }
            });

            client.query('SELECT * FROM transaction ORDER BY transactionid desc limit 1', null, function(err, result) {
                done();

                if (err) {
                    return callback(null, err);
                }
                if (result.rows.length > 0) {

                    var transaction = new Transaction();

                    transaction.userid= result.rows[0]['userid'];
                    transaction.coin = result.rows[0]['coin'];
                    transaction.pending = result.rows[0]['pending'];
                    transaction.amount = result.rows[0]['amount'];
                    transaction.date = result.rows[0]['date'];
                    transaction.transactionid = result.rows[0]['transactionid'];

                    callback(transaction, null);
                }
            });
        });
    };
};

Transaction.find = function(email, coin, callback) {
        pool.connect((err, client, done) => {
            if (err) {
                return console.error('Could not connect to Postgres.', err);
            }
            let query = `SELECT transaction.*, coin.abbreviation
                        FROM users
                        INNER JOIN transaction
                        ON users.userid = transaction.userid
                        INNER JOIN coin
                        ON coin.coinid = transaction.coinid
                        WHERE users.email='${email}'`;
            if (coin !== -1) {
                query += `
                AND
                coin.coinid='${coin}'`;
            }
            query += `
            ORDER BY transaction.date DESC`;
            
            client.query(query, function(err, result) {
                done();

                if (err) {
                    return callback(err, this);
                }
                if (result.rows.length > 0) {
                    var transactions = result.rows.map((r) => {
                        var t = new Transaction();

                        t.userid= r['userid'];
                        t.abbreviation = r['abbreviation'];
                        t.pending = r['pending'];
                        t.amount = r['amount'];
                        t.date = r['date'];
                        t.transactionid = r['transactionid'];

                        return t;
                    });

                    return callback(null, transactions);
                } else {
                    return callback(false, null);
                }
            });
        });
    };


Transaction.balances = function(email, callback) {
        pool.connect((err, client, done) => {
            if (err) {
                return console.error('Could not connect to Postgres.', err);
            }
            let query = `
            SELECT coin.abbreviation, COALESCE(b.balance, 0) as balance
            FROM coin
            CROSS JOIN users
            LEFT JOIN
                (SELECT SUM(transaction.amount) as balance, 
                 transaction.coinid,
                 transaction.userid
                 FROM transaction
                 WHERE transaction.pending=false
                 GROUP BY transaction.coinid, transaction.userid
                ) b
            ON b.coinid = coin.coinid
            AND
            b.userid = users.userid
            WHERE users.email='${email}'`;

            client.query(query, function(err, result) {
                done();

                if (err) {
                    return callback(err, this);
                }
                if (result.rows.length > 0) {
                    var balances = result.rows.map((r) => {
                        var t = {};
                        t.abbreviation = r['abbreviation'];
                        t.balance = r['balance'];

                        return t;
                    });

                    return callback(null, balances);
                } else {
                    return callback(false, null);
                }
            });
        });
    };

module.exports = Transaction;