const pg = require('pg');
var bcrypt = require('bcrypt-nodejs');

pg.defaults.ssl = true;

let pool = new pg.Pool({connectionString: process.env.DATABASE_URL}); 

class User {

    constructor(email, password) {
        
        this.email = email;
        this.password = password;
    }

    comparePassword(password, cb) {
        bcrypt.compare(password, this.password, function (err, isMatch) {
            if (err) {
                return cb(err);
            }
            cb(null, isMatch);
        })
    };
    save(callback) {
        let email = this.email;
        let password = this.password
        pool.connect((err, client, done) => {
            if (err) {
                return console.error('Could not connect to Postgres.', err);
            }

            bcrypt.genSalt(10, function (err, salt) {
                if (err) {
                    return callback(err)
                }
                bcrypt.hash(password, salt, null, function (err, hash) {
                    if (err) {
                        return callback(err);
                    }
                    client.query('INSERT INTO users(email, password) VALUES($1, $2)', [email, hash], function (err, result) {
                        done();
                        if(err){
                            return console.error('error running query', err);
                        }
                    });

                    client.query('SELECT * FROM users ORDER BY userid desc limit 1', null, function(err, result) {
                        done();

                        if (err) {
                            console.log(err);
                            return callback(null);
                        }
                        if (result.rows.length > 0) {

                            var user = new User();

                            user.email= result.rows[0]['email'];
                            user.password = result.rows[0]['password'];
                            user.userid = result.rows[0]['userid'];
                            user.admin = result.rows[0]['admin'];

                            callback(user);

                        }
                    });
                })
            })
        });
    };
};

User.findOne = function(email, callback){
        pool.connect((err, client, done) => {
            if (err) {
                return console.error('Could not connect to Postgres.', err);
            }
            client.query("SELECT * from users where email=$1", [email], function(err, result) {
                done();

                if (err) {
                    return callback(err, false, this);
                }
                if (result.rows.length > 0) {
                    var user = new User();

                    user.email = result.rows[0]['email'];
                    user.password = result.rows[0]['password'];
                    user.userid = result.rows[0]['userid'];
                    user.admin = result.rows[0]['admin'];

                    return callback(null, user);
                } else {
                    return callback(false, null);
                }
            });
        });
    };

module.exports = User;